<?php

namespace App\Http\Controllers\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Http\Controllers\Conversations\SelectServicesConversation;
use App\Http\Controllers\Conversations\ConcernsConversation;
use Auth;

class SelectInquiryConversation extends Conversation
{
    public function askInquiry()
    {
        $question = Question::create('How can we help you?')
            ->callbackId('select_inquiry')
            ->addButtons([
                Button::create('I want to avail your product and services.')->value('Product'),
                Button::create('I have inquiries and other concerns.')->value('Concerns'), 
            ]);

        $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->userStorage()->save([
                    'name' => Auth::user()->name,
                    'email' => Auth::user()->email,
                    'inquiry' => $answer->getValue(),
                ]);

                switch ($answer->getValue()) {
                    case 'Product':
                        $this->bot->startConversation(new SelectServicesConversation());
                        break;

                    case 'Concerns':
                        $this->bot->startConversation(new ConcernsConversation());
                        break;
                    
                    default:
                        # code...
                        break;
                }

                //$this->bot->startConversation(new DateTimeConversation());
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askInquiry();
    }
}