<?php

namespace App\Http\Controllers\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Http\Controllers\Conversations\ConfirmationConversation;

class ConcernsConversation extends Conversation
{
    public function concerns()
    {
        $question = Question::create('What are your inquiry or concerns?')
            ->callbackId('post_concern');

        $this->ask($question, function (Answer $answer) {
            $this->bot->userStorage()->save([
                'concerns' => $answer->getValue()
            ]);
            $this->bot->startConversation(new ConfirmationConversation());
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->concerns();
    }
}