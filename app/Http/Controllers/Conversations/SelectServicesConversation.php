<?php

namespace App\Http\Controllers\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Http\Controllers\Conversations\ConfirmationConversation;

class SelectServicesConversation extends Conversation
{
    public function services()
    {
        $question = Question::create('What product do you want to avail?')
            ->callbackId('select_services')
            ->addButtons([
                Button::create('Power User - Perfect for solopreneurs')->value('Power User'),
                Button::create('Build - Great for small teams')->value('Build'), 
                Button::create('Grow - Best for growing teams')->value('Grow'), 
                Button::create('Play Big - Made for agencies')->value('Play Big'), 
            ]);

        $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->userStorage()->save([
                    'product' => $answer->getValue(),
                ]);
                $this->bot->startConversation(new ConfirmationConversation());
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->services();
    }
}