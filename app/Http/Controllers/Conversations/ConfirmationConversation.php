<?php

namespace App\Http\Controllers\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use App\Inquiry;
use Auth;

class ConfirmationConversation extends Conversation
{
    public function confirm()
    {
        $user = $this->bot->userStorage()->find();

        $message = 'Name : '.$user->get('name').'<br>';
        $message .= 'Email : '.$user->get('email').'<br>';
        $message .= 'Inquiry : '.$user->get('inquiry').'<br>';
        if( $user->get('product') ){
            $message .= 'Product : '.$user->get('product').'<br>';
        } else if( $user->get('concerns') ) {
            $message .= 'Concerns : '.$user->get('concerns').'<br>';
        }

        $this->say('Thank you! We will message you soon. <br><br>'.$message);

        Inquiry::create([
            'user_id' => Auth::user()->user_id,
            'inquiry' => $user->get('inquiry'),
            'message' => ( $user->get('product') ? $user->get('product') : $user->get('concerns') )
        ]);
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->confirm();
    }
}