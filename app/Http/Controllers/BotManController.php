<?php
  
namespace App\Http\Controllers;
  
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Http\Controllers\Conversations\SelectInquiryConversation;
  
class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
  
        $botman->hears('{message}', function($botman, $message) {
            if ($message == 'Lets plann!') {
                $this->startConversation($botman);
            } else {
                $botman->reply("Write 'Lets plann!' to start.");
            }
        });
        
        $botman->listen();
    }

    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new SelectInquiryConversation());
    }
}