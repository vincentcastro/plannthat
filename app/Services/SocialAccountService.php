<?php

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\SocialAccount;
use App\User;

class SocialAccountService
{
    public function createOrGetUser($provider, ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $email = $providerUser->getEmail();
            if(!$email){
                $email = $providerUser->getId().'@plannthat.com';
            } 
            $user = User::where('email',$email)->first();

            if (!$user) {
                $avatar = '';
                if($provider == "facebook"){
                    $avatar = "https://graph.facebook.com/".$providerUser->getId()."/picture?type=large";
                } else if($provider == "twitter"){
                    $avatar = $providerUser->avatar_original;
                }

                $user = User::create([
                    'email' => $email,
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1,10000)),
                    'avatar' => $avatar
                ]);

            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}