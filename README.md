## Plannthat Instruction

- .env.localhost for .env
- create database named 'plannthat'

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
