@extends('layouts.app')
@section('styles')
<style>
    html, body {
        background-image: none !important;
        font-family: 'Nunito', sans-serif !important;
    }
</style>
@endsection
@section('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
<script>
    var botmanWidget = {
        title: 'Plannthat Chat Bot',
        aboutLink: 'http://plannthat.com',
        mainColor: '#99cff1',
        bubbleBackground: '#99cff1',
        aboutText: 'Plannthat Dev Test',
        introMessage: "✋ Hi {{ auth()->user()->name }}! Welcome to Plannthat!"
    };
</script>

<script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>
                        <div>
                            <img
                            src="{{auth()->user()->avatar}}"
                            style="border-radius: 50%; padding-bottom: 20px; width: 150px;"
                            />
                        </div>
                        <div>
                            <strong>{{auth()->user()->name}}</strong>
                        </div>
                        <div>
                            <small>{{auth()->user()->email}}</small>
                        </div>
                    </center>
                    

                    @if(count($inquiries))
                        <hr />
                        <h3> Inquiries </h3>
                        <table class="table">
                        <thead>
                            <tr>
                                <th> </th>
                                <th> Name </th>
                                <th> Email </th>
                                <th> Inquiry </th>
                                <th> Message </th>
                            </tr>
                        </thead>
                        @foreach($inquiries as $inquiry)
                            <tr>
                                <td> 
                                    <img
                                        src="{{ $inquiry->user->avatar }} "
                                        style="border-radius: 50%; width: 30px"
                                    />
                                </td>
                                <td> {{ $inquiry->user->name }} </td>
                                <td> {{ $inquiry->user->email }} </td>
                                <td> {{ $inquiry->inquiry }} </td>
                                <td> {{ $inquiry->message }} </td>
                            </tr>
                        @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
